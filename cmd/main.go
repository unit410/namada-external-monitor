package main

import (
	"fmt"
	"log"
	"net/http"
	"time"

	"github.com/samber/lo"
	namada "gitlab.com/unit410/namada-external-monitor/pkg/monitor/namada"
)

func serve() {
	listenAddr := fmt.Sprintf(":%d", 8080)

	mux := http.NewServeMux()

	mux.HandleFunc("/", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusOK)
	})

	mux.HandleFunc("/namada", func(w http.ResponseWriter, r *http.Request) {
		defer func() {
			if err := recover(); err != nil {
				fmt.Println("panic occurred while attempting to run Namada Monitor", err)
				w.WriteHeader(http.StatusInternalServerError)
				return
			}
		}()
		namada.Run()
		w.WriteHeader(http.StatusOK)
	})

	server := &http.Server{
		Addr:              listenAddr,
		ReadHeaderTimeout: 3 * time.Second,
		Handler:           mux,
	}
	log.Printf("Listening on %s", listenAddr)
	lo.Must0(server.ListenAndServe())
}

func main() {
	println("Starting External Monitor")
	serve()
}
