GO_MAIN_PACKAGE ?= ./cmd 
CGO_ENABLED ?= 0
GOPRIVATE ?= go.u4hq.dev
GO ?= CGO_ENABLED=$(CGO_ENABLED) GOPRIVATE=$(GOPRIVATE) go

BINARY_NAME ?= $(notdir $(realpath .))

build: bin/$(BINARY_NAME) ## Build the application binary

bin/$(BINARY_NAME): $(GO_SRC)
	$(GO) build -mod=readonly -o '$(abspath $@)' $(GO_FLAGS) $(GO_BUILD_FLAGS) $(GO_MAIN_PACKAGE)

build-docker: Dockerfile $(GO_SRC) 
	docker build . $(BUILD_ARGS) --platform linux/amd64 --progress plain --no-cache --tag '$(TAG)'
	mkdir -p '$(dir $@)'
	echo '$(TAG)' > '$@'

lint:
	$(GO) mod tidy
	$(GO) fmt ./...
	golangci-lint run -c ./.golangci.yml
