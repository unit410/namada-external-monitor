module gitlab.com/unit410/namada-external-monitor

go 1.20

require (
	github.com/samber/lo v1.38.1
	github.com/slack-go/slack v0.12.2
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	github.com/gorilla/websocket v1.4.2 // indirect
	github.com/stretchr/testify v1.8.1 // indirect
	golang.org/x/exp v0.0.0-20220303212507-bbda1eaf7a17 // indirect
)
