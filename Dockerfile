FROM golang:1.19 AS build

COPY . /src
WORKDIR /src
RUN go env -w GOPRIVATE="gitlab.com/unit410"
RUN make build BINARY_NAME=monitor CGO_ENABLED=1
RUN mv /src/bin/monitor /bin/monitor
ENTRYPOINT ["/bin/monitor"]
