# Namada External Monitor

:warning: This repo is intended to be forked, no incoming MRs will be merged :warning:

A simple Namada service for checking status of Namada validators. The service runs an http server and accepts traffic on port 8080.
Two endpoints are supported
- Healthcheck: :8080/ -> returns 200 if the service is up and running
- Monitoring: :8080/namada -> Runs the core monitoring software checking the status of namada validators 


## Architecture 
- The external monitor relies on two RPCs endpoints. One that is the native namada RPC and one that is a ping middleware which supports some cosmos endpoints
- The external monitor stores all of its state locally to reduce redundant alarms
- If the monitor hits an alerting state, it will leverage slack webhooks and PD incidents
- To kick off a run, hit the endpoint <host>:8080/namada
- To run this at a set interval, setup a cron job that curls the above endpoint every N minutes


![Arch Diagram](./namada-external-monitor-arch.png)
## Local Development

```shell
scripts/run
```

## Build

```shell
make build
```

## Docker Build
```shell
TAG=v1.0.0 make build-docker
```

## Run
```shell
docker run -e CONFIG="$(cat etc/testnet.json)" -p 8080:8080 v1.0.0
```

## Lint
```shell
make lint
```

## Config 
| env                        | description                                                                    |
| -------------------------- | -------------------------------------------------------------------------------|
| namada_rpc_url             | Namada RPC URL                                                                 |
| namada_ping_url            | Namada Ping URL (https://github.com/vknowable/namada-ping-middleware)          |
| namada_validator_addresses | Map of ValidatorAddress -> ConsensusAddress                                    |
| slack_channel              | Name of the slack channel for messages                                         |
| slack_url                  | Slack webhook url that can accept messages                                     |
| pagerduty_key              | API key to create PD incidents                                                 |
| slack_enabled              | Bool to enable/disable slack messages                                          |
| pagerduty_enabled          | Bool to enable/disable creating PD incidents                                   |
| environment                | Used in slack, PD, and datastore to isolate between environments               |
| block_starting_point       | Where to start block processing, if 0 in config only gets latest block         |
| proposal_starting_point    | Where to start proposal processing                                             |

## Monitoring 
### Validators
- Verify you are signing each block - Slack and PD 
- Verify you are not jailed - Slack and PD 
- Verify your delegator shares is greater than 0 - Slack and PD
- Verify you bond status == BOND_STATUS_BONDED - Slack

### Proposals
- Keeps track of proposals and slacks for new ones

### RPC Node Health
- Verifies the node is synced, will not proceed in the run if the node is not yet synced to prevent false alarms. 

### Misc Errors
- If the monitor encounters errors 5 runs in a row, it will create a PD alert 

### Github Releases
- Tracks the latest github release in state and if it changes, it will send a slack message. Will print the latest version on start up. 