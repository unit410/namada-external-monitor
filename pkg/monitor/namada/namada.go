package monitor

import (
	"fmt"
	"strconv"

	"github.com/samber/lo"
)

var config Config

func Run() {
	initializeMonitor()

	catchingUp := getDaemonStatus()

	if catchingUp {
		fmt.Println("Node is not synced, skipping monitor run")
		return
	}

	verifyBlockSignatures()
	checkLatestProposals()
	verifyLatestRelease()
	// This monitor takes minutes to run, the endpoint doesnt support pagination and we need to get all the validators in all states. Support needs to be added in the ping middleware for validators/<validator_address>
	verifyValidatorDetails()
	validateRun()

	fmt.Println("Monitoring run complete for Namada")
}

func initializeMonitor() {
	println("Initializing Monitor")
	config = loadConfig()
	println("Config Loaded")
	initializePagerDuty()
	println("PagerDuty and Datastore Initialized")
}

func getDaemonStatus() bool {
	status := getRequest[Status](config.NamadaRPCUrl + "/status")
	state := GetState()

	if state.CatchingUp != status.Result.SyncInfo.CatchingUp {
		SendSlackMessage(fmt.Sprintf("RPC Node catchingUp: %s", strconv.FormatBool(status.Result.SyncInfo.CatchingUp)))
		state.CatchingUp = status.Result.SyncInfo.CatchingUp
		UpdateState(state)
	}
	return status.Result.SyncInfo.CatchingUp
}

func verifyLatestRelease() {
	state := GetState()
	latestTag := getRequest[GithubRelease]("https://api.github.com/repos/anoma/namada/releases/latest").TagName

	if latestTag != state.LastGithubRelease {
		SendSlackMessage("New version of namada has been released! tag:" + latestTag)
		state.LastGithubRelease = latestTag
		UpdateState(state)
	}
}

func verifyBlockSignatures() {
	state := GetState()
	latestSeenBlock := state.LatestSeenBlock
	currentBlockNumber := lo.Must(strconv.Atoi(getRequest[Block](config.NamadaRPCUrl + "/block").Result.Block.Header.Height))

	// If state is 0, meaning first run, just grab the latest block unless specified in the config
	if latestSeenBlock == 0 {
		if loadConfig().BlockStartingPoint == 0 {
			latestSeenBlock = currentBlockNumber - 1
		} else {
			latestSeenBlock = loadConfig().BlockStartingPoint
		}
	}
	monitoredValidators := lo.Values(loadConfig().NamadaValidatorAddresses)

	// Loop over each block from latestSeenBlock to currentBlockNumber
	for blockNum := latestSeenBlock + 1; blockNum <= currentBlockNumber; blockNum++ {
		block := getRequest[Block](fmt.Sprintf("%s/block?height=%d", config.NamadaRPCUrl, blockNum))
		// Extract validator addresses who signed the current block
		signedValidators := lo.Map(block.Result.Block.LastCommit.Signatures, func(sig Signature, index int) string {
			return sig.ValidatorAddress
		})

		// Check each monitored validator to see if they have signed the block
		for _, mVal := range monitoredValidators {
			if !lo.Contains(signedValidators, mVal) {
				CreatePagerDutyIncidentForValidator(fmt.Sprintf("Validator is not signing blocks: %s at block height: %d\n", mVal, blockNum), "signing_blocks", mVal)
			}
		}
	}

	// Update the state with the latest block number seen
	state.LatestSeenBlock = currentBlockNumber
	UpdateState(state)
}

func checkLatestProposals() {
	proposals := getRequest[Proposals](config.NamadaPingUrl + "/cosmos/gov/v1beta1/proposals")
	state := GetState()

	// first run, use config starting point
	if state.LastSeenProposal == 0 && config.ProposalStartingPoint != 0 {
		state.LastSeenProposal = config.BlockStartingPoint
		UpdateState(state)
	}

	proposalIds := lo.Map(proposals.Proposals, func(prop Proposal, index int) string {
		return prop.ProposalID
	})

	newProposals := lo.Filter(proposalIds, func(prop string, index int) bool {
		return lo.Must(strconv.Atoi(prop)) > state.LastSeenProposal
	})

	for _, prop := range newProposals {
		proposal := getProposalById(prop)
		message := fmt.Sprintf(`New Proposal Created: 
		ID: %s 
		Type: %s 
		Title: %s 
		Description: %s
		Start: %s 
		End: %s 
		`,
			prop,
			proposal.Proposal.Content.Type,
			proposal.Proposal.Content.Title,
			proposal.Proposal.Content.Description,
			proposal.Proposal.VotingStartTime,
			proposal.Proposal.VotingEndTime)

		SendSlackMessage(message)
	}

	if len(newProposals) > 0 {
		last := newProposals[len(newProposals)-1:]
		state.LastSeenProposal = lo.Must(strconv.Atoi(last[0]))
		UpdateState(state)
	}
}

func getProposalById(id string) ProposalDetails {
	return getRequest[ProposalDetails](config.NamadaPingUrl + "/cosmos/gov/v1beta1/proposals/" + id)
}

func verifyValidatorDetails() {
	validators := getRequest[Validators](config.NamadaPingUrl + "/cosmos/staking/v1beta1/validators")
	monitoredValidators := lo.Keys(loadConfig().NamadaValidatorAddresses)

	filteredValidators := lo.Filter(validators.Validators, func(validator Validator, index int) bool {
		return lo.Contains(monitoredValidators, validator.OperatorAddress)
	})

	for i := range filteredValidators {
		validator := &filteredValidators[i]

		if validator.Jailed {
			CreatePagerDutyIncidentForValidator(fmt.Sprintf("Validator is jailed: %s \n", validator.OperatorAddress), "jailed", validator.OperatorAddress)
		}

		if validator.Status != "BOND_STATUS_BONDED" {
			SendSlackMessage(fmt.Sprintf("Validator bond status is not `BOND_STATUS_BONDED`: %s", validator.OperatorAddress))
		}

		if lo.Must(strconv.ParseFloat(validator.DelegatorShares, 64)) <= 0 {
			CreatePagerDutyIncidentForValidator(fmt.Sprintf("Delegator Shares are 0 for: %s", validator.OperatorAddress), "jailed", validator.OperatorAddress)
		}
	}
}

func validateRun() {
	state := GetState()
	if state.CurrentRunErrorCount != 0 {
		state.PreviousRunsWithError += 1
		if state.PreviousRunsWithError >= 5 {
			SendSlackMessage("Namada External Monitor has had 5 consecutive runs with errors")
			state.PreviousRunsWithError = 0
		}
	} else {
		state.PreviousRunsWithError = 0
	}
	state.CurrentRunErrorCount = 0
	UpdateState(state)
}
