package monitor

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"

	"github.com/samber/lo"
	alerts "gitlab.com/unit410/namada-external-monitor/pkg/alerts"
	ds "gitlab.com/unit410/namada-external-monitor/pkg/datastore"
)

var pd alerts.PagerDutyClient

var stateKey string = "namada-state"
var pubKey string = "namada-pubkey"

// Slack & PD
func initializePagerDuty() {
	if config.PagerdutyEnabled {
		pd = *alerts.CreatePagerDutyClient(config.PagerdutyKey)
	}
}

func CreatePagerDutyIncidentForValidator(text string, tag string, validator string) {
	validatorState := GetValidatorState(validator)
	currentTime := time.Now()

	if lastSent, exists := validatorState.SentPagerdutyAlerts[tag]; exists {
		// Check if it's been more than 10 minutes since the last alert
		lastSentTime := lo.Must(time.Parse(time.RFC3339, lastSent))
		if currentTime.Sub(lastSentTime).Minutes() <= 10 {
			fmt.Printf("PagerDuty Alert for '%s' on validator '%s' was already created less than 10 minutes ago, skipping\n", tag, validator)
			return
		}
	}

	if config.PagerdutyEnabled {
		SendSlackMessage(fmt.Sprintf("Creating PagerDuty Alert: %s \n", text))
		lo.Must(pd.CreateIncident(fmt.Sprintf("Namada External Monitor - %s - %s", config.Environment, text), text, "Error", config.Environment))
		// Update the SentPagerdutyAlerts map with the current time
		validatorState.SentPagerdutyAlerts[tag] = currentTime.Format(time.RFC3339)
		UpdateValidatorState(validator, validatorState)
	} else {
		fmt.Printf("PagerDuty is Disabled, would create: %s \n", text)
		// For debugging purposes, even though its disabled, lets not constantly print we would create the incident
		validatorState.SentPagerdutyAlerts[tag] = currentTime.Format(time.RFC3339)
		UpdateValidatorState(validator, validatorState)
	}
}

func SendSlackMessage(text string) {
	// Throttling is handled in the slack function
	if config.SlackEnabled {
		fmt.Printf("Sending message to slack channel %s message: %s \n", config.SlackChannel, text)
		alerts.Slack(text, config.SlackChannel, config.SlackUrl)
	} else {
		fmt.Printf("Slack is Disabled, would send to slack channel: %s message: %s \n", config.SlackChannel, text)
	}
}

// State Management
func GetState() (state State) {
	err := ds.Get(context.Background(), stateKey, config.Environment, &state)
	handleError("Error getting state", err)
	return state
}

func UpdateState(updated State) {
	err := ds.Store(context.Background(), stateKey, config.Environment, updated)
	handleError("Error updating state", err)
}

func GetValidatorState(validator string) (state ValidatorAlertingState) {
	err := ds.Get(context.Background(), fmt.Sprintf("%s-%s", pubKey, validator), config.Environment, &state)
	if err != nil {
		handleError("Error getting validator state", err)
		state = ValidatorAlertingState{
			SentPagerdutyAlerts: make(map[string]string),
		}
	}

	return state
}

func UpdateValidatorState(validator string, updated ValidatorAlertingState) {
	err := ds.Store(context.Background(), fmt.Sprintf("%s-%s", pubKey, validator), config.Environment, updated)
	handleError("Error updating validator state", err)
}

// Config
func loadConfig() Config {
	configFile := os.Getenv("CONFIG")
	lo.Must0(json.Unmarshal([]byte(configFile), &config))
	return config
}

// Error handling
func handleError(text string, err error) {
	if err == nil {
		return
	}
	state := GetState()
	fmt.Printf("Error encountered during monitor run. \n message: %s \n error: %e", text, err)
	state.CurrentRunErrorCount += 1
	UpdateState(state)
}

// API
func getRequest[T any](requestPath string) (result T) {
	// #nosec G107
	response, err := http.Get(requestPath)

	if err != nil {
		handleError("panic occurred while making api request", err)
	}

	defer func() {
		_ = response.Body.Close()
	}()

	body := lo.Must(io.ReadAll(response.Body))

	if response.StatusCode != http.StatusOK {
		handleError(fmt.Sprintf("http status (%d) not ok for %s", response.StatusCode, requestPath), nil)
		return
	}

	if err := json.Unmarshal(body, &result); err != nil {
		handleError(fmt.Sprintf("error unmarshalling response for %s", requestPath), err)
		return
	}

	return result
}
