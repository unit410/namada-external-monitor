package datastore

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
)

func Store(ctx context.Context, name string, environment string, data interface{}) (returnErr error) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("panic occurred while storing data to file:", err)
			returnErr = fmt.Errorf("panic occurred while storing data to file")
			return
		}
	}()

	// Generate the file name based on environment and name
	fileName := fmt.Sprintf("namada-state-%s:%s.json", environment, name)

	// Marshal the data into JSON
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("error marshaling data: %w", err)
	}

	// Write the data to a file
	err = os.WriteFile(fileName, dataBytes, 0o600) // Adjust the permissions as needed
	if err != nil {
		return fmt.Errorf("error writing data to file: %w", err)
	}

	return nil
}

func Get(ctx context.Context, name string, environment string, result interface{}) (returnErr error) {
	defer func() {
		if err := recover(); err != nil {
			fmt.Println("panic occurred while reading data from file:", err)
			returnErr = fmt.Errorf("panic occurred while reading data from file")
			return
		}
	}()

	// Generate the file name based on environment and name
	fileName := fmt.Sprintf("namada-state-%s:%s.json", environment, name)

	// Read the data from the file
	dataBytes, err := os.ReadFile(fileName)
	if err != nil {
		if os.IsNotExist(err) {
			return fmt.Errorf("entity not found")
		}
		return fmt.Errorf("error reading data from file: %w", err)
	}

	// Unmarshal the data into the result
	err = json.Unmarshal(dataBytes, &result)
	if err != nil {
		return fmt.Errorf("error unmarshaling data: %w", err)
	}

	return nil
}
