package monitor

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
)

const (
	v2eventEndPoint = "https://events.pagerduty.com/v2/enqueue"
)

type (
	// V2Event includes the incident/alert details
	V2Event struct {
		RoutingKey string        `json:"routing_key"`
		Action     string        `json:"event_action"`
		DedupKey   string        `json:"dedup_key,omitempty"`
		Images     []interface{} `json:"images,omitempty"`
		Client     string        `json:"client,omitempty"`
		ClientURL  string        `json:"client_url,omitempty"`
		Payload    *V2Payload    `json:"payload,omitempty"`
	}

	// V2Payload represents the individual event details for an event
	V2Payload struct {
		Summary   string      `json:"summary"`
		Source    string      `json:"source"`
		Severity  string      `json:"severity"`
		Timestamp string      `json:"timestamp,omitempty"`
		Component string      `json:"component,omitempty"`
		Group     string      `json:"group,omitempty"`
		Class     string      `json:"class,omitempty"`
		Details   interface{} `json:"custom_details,omitempty"`
	}

	V2EventResponse struct {
		RoutingKey  string `json:"routing_key"`
		DedupKey    string `json:"dedup_key"`
		EventAction string `json:"event_action"`
	}

	// PagerDutyClient is struct that contains a routingKey and incidents tracked via a mapping
	PagerDutyClient struct {
		routingKey string
		incidents  map[string]string
	}
)

// New returns a new PagerDutyClient given a service routing key
// and en empty mapping of incidents
func CreatePagerDutyClient(key string) *PagerDutyClient {
	i := make(map[string]string)

	return &PagerDutyClient{
		routingKey: key,
		incidents:  i,
	}
}

// GetOpenIncident returns the dedupe key of any open incidents matching
// the given kind
func (pd *PagerDutyClient) GetOpenIncident(kind string) string {
	return pd.incidents[kind]
}

// SetOpenIncident sets the dedupe key of an incidents matching
// the given kind
func (pd *PagerDutyClient) SetOpenIncident(kind, incident string) {
	pd.incidents[kind] = incident
}

// CreateIncident creates a new PagerDuty incident with the given title and body. The kind
// is used internally to track active incidents (of the particular kind). This allows the
// monitor to resolve incidents that it created and prevents multiple incidents of the same
// kind from being created.
func (pd *PagerDutyClient) CreateIncident(title, body, kind string, environment string) (string, error) {
	dedupekey := pd.GetOpenIncident(kind)

	payload := &V2Payload{
		Summary:   title,
		Source:    fmt.Sprintf("Namada External Monitor - %s", environment),
		Severity:  "critical",
		Component: "",
		Group:     "",
		Class:     "",
		Details:   body,
	}

	event := V2Event{
		RoutingKey: pd.routingKey,
		Action:     "trigger",
		DedupKey:   dedupekey,
		Images:     nil,
		Client:     "",
		ClientURL:  "",
		Payload:    payload,
	}

	resp, err := manageV2Event(event)
	if err != nil {
		return "", err
	}

	pd.SetOpenIncident(kind, resp.DedupKey)

	log.Printf("[PagerDuty]\tincident=%v", resp)
	return resp.DedupKey, nil
}

func manageV2Event(e V2Event) (*V2EventResponse, error) {
	data, err := json.Marshal(e)
	if err != nil {
		return nil, err
	}

	req, reqErr := http.NewRequest("POST", v2eventEndPoint, bytes.NewBuffer(data))
	if reqErr != nil {
		return nil, reqErr
	}

	req.Header.Set("Content-Type", "application/json")

	resp, doErr := http.DefaultClient.Do(req)
	if doErr != nil {
		return nil, doErr
	}

	defer resp.Body.Close()

	if resp.StatusCode != http.StatusAccepted {
		bytes, err := io.ReadAll(resp.Body)
		if err != nil {
			return nil, fmt.Errorf("HTTP Status Code: %d", resp.StatusCode)
		}

		return nil, fmt.Errorf("HTTP Status Code: %d, Message: %s", resp.StatusCode, string(bytes))
	}

	var eventResponse V2EventResponse
	if err := json.NewDecoder(resp.Body).Decode(&eventResponse); err != nil {
		return nil, err
	}

	return &eventResponse, nil
}
