package monitor

import (
	"fmt"
	"log"
	"time"

	"github.com/slack-go/slack"
)

var (
	throttleDurationMinutes = 10 * time.Minute
	alertThrottle           map[string]*time.Time
)

// Slack a message
func Slack(text string, channel string, slackUrl string) {
	msg := &slack.WebhookMessage{
		Text: text,
	}

	// Throttle
	if HasAlreadyAlerted(msg.Text+msg.Channel, throttleDurationMinutes) {
		log.Println("Already alerted.  Not posting again.")
		return
	}
	// Post
	err := slack.PostWebhook(slackUrl, msg)
	if err != nil {
		fmt.Printf("Error posting slack webhook:\n %s \n", err.Error())
	}
}

func HasAlreadyAlerted(message string, duration time.Duration) bool {
	if alertThrottle == nil {
		alertThrottle = map[string]*time.Time{}
	}

	lastAlert := alertThrottle[message]

	now := time.Now()
	if lastAlert == nil || now.Sub(*lastAlert) > duration {
		alertThrottle[message] = &now
		return false
	}

	return true
}
